# Cashier Route

	+ Notes: Variables Used

		DEVICE_ID: Device id created in Magento Admin
		DEVICE_KEY_IN_MD5: MD5 string of Deviced Key created in Magento Admin
		BASE_URL: Web base url
		BASE64_ENCODED_IMAGE: string generated from base64_encode image.

			Example PHP code:
				$path = '/path/to/image.png';
				$type = pathinfo($path, PATHINFO_EXTENSION);
				$data = file_get_contents($path);
				$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
				echo $base64; 

	+ Config

		Request Format: 

			curl -X POST -H "Content-Type: application/json" -d '{
			  "device_id": "DEVICE_ID",
			  "device_key": "DEVICE_KEY_IN_MD5"
			}' "BASE_URL/xposrest/config/

		Updated Response fields:
			{	...
				"result": {
					...
				    "manage_device": {
				    	...
				      	"sales_person": {
					        "entity_id": "120",
					        "entity_type_id": "1",
					        "attribute_set_id": "0",
					        "website_id": "1",
					        "email": "test@test.com",
					        "group_id": "6",
					        "increment_id": null,
					        "store_id": "0",
					        "created_at": "2017-03-11T22:31:24-08:00",
					        "updated_at": "2017-03-12 06:31:24",
					        "is_active": "1",
					        "disable_auto_group_change": "0",
					        "created_in": "Admin",
					        "prefix": null,
					        "firstname": "test",
					        "middlename": null,
					        "lastname": "test",
					        "suffix": null,
					        "password_hash": "81a6a47c8abc04d28e0564cacf61827c:nmUWvednl7jNSCSo2tZ6lS74EDGigsaE",
					        "taxvat": null,
					        "cellphonenumber": null,
					        "driver_licence": "0"
					      },
					    "routes": [
					        {
					          "station_id": "1",
					          "route_id": "1",
					          "routename": "test 1",
					          "day": "Sunday",
					          "salesperson_name": "120",
					          "total_stop": "2",
					          "stops": {
					            "totalRecords": 1,
					            "items": [
					              {
					                "stop_id": "1",
					                "sortorder": "1",
					                "stopname": "RYLAND TEXACO",
					                "route_id": "1",
					                "salesperson_name": "120",
					                "day": "Sunday",
					                "address": null
					              },
					              {
					                "stop_id": "2",
					                "sortorder": "2",
					                "stopname": "GREENBRIER FUEL CITY",
					                "route_id": "1",
					                "salesperson_name": "120",
					                "day": "Sunday",
					                "address": null
					              }
					            ]
					          }
					        },
					        {
					          "station_id": "2",
					          "route_id": "2",
					          "routename": "test 2",
					          "day": "Wednesday",
					          "salesperson_name": "120",
					          "total_stop": "1",
					          "stops": {
					            "totalRecords": 1,
					            "items": [
					              {
					                "stop_id": "3",
					                "sortorder": "1",
					                "stopname": "sc computer",
					                "route_id": "2",
					                "salesperson_name": "120",
					                "day": "Wednesday",
					                "address": null
					              }
					            ]
					          }
					        }
					      ]
				    	...
					},
					"all_sales_person": [
				      {
				        "entity_id": "120",
				        "entity_type_id": "1",
				        "attribute_set_id": "0",
				        "website_id": "1",
				        "email": "test@test.com",
				        "group_id": "6",
				        "increment_id": null,
				        "store_id": "0",
				        "created_at": "2017-03-12 06:31:24",
				        "updated_at": "2017-03-12 06:31:24",
				        "is_active": "1",
				        "disable_auto_group_change": "0",
				        "prefix": null,
				        "firstname": "test",
				        "middlename": null,
				        "lastname": "test",
				        "suffix": null,
				        "taxvat": null,
				        "cellphonenumber": null,
				        "password_hash": "81a6a47c8abc04d28e0564cacf61827c:nmUWvednl7jNSCSo2tZ6lS74EDGigsaE",
				        "created_in": "Admin",
				        "driver_licence": "0"
				      }
				    ]		
		        	...
				}
				...
			}

    + Create Order

    	Updated Request fields:

			{
				"sales_person_id": "120",
  				"signature" : "BASE64_ENCODED_IMAGE",
  				"location" : "loaction data",
  				"route_id" : ROUTE_ID,
				"stop_id" : STOP_ID,
				"route_date" : "ROUTE_DATE",
			}

	+ Order History

		Request Format:

			curl -X POST -H "Content-Type: application/json" -H -d '{
			  "device_id": "DEVICE_ID",
			  "device_key": "DEVICE_KEY_IN_MD5",
			  "product_id": "1125",
			  "customer_id": "25"
			}' "BASE_URL/xposrest/order/history/"

		Example Response:

			{
			  "status": "success",
			  "result": {
			    "totalRecords": 1,
			    "items": [
			      {
			        "order_id": "419",
			        "date": "2017-03-12 08:39:12",
			        "qty": "1.0000",
			        "price": "3.9900"
			      },
			      {
			        "order_id": "418",
			        "date": "2017-03-12 08:36:30",
			        "qty": "1.0000",
			        "price": "3.9900"
			      },
			      {
			        "order_id": "401",
			        "date": "2017-03-06 17:44:45",
			        "qty": "1.0000",
			        "price": "3.9900"
			      },
			      {
			        "order_id": "399",
			        "date": "2017-03-06 17:22:58",
			        "qty": "1.0000",
			        "price": "3.9900"
			      }
			    ]
			  },
			  "message": ""
			}

	+ Route Api

		Request Format:

			curl -X POST -H "Content-Type: application/json" -d '{
			  "device_id": "DEVICE_ID",
			  "device_key": "DEVICE_KEY_IN_MD5",
			  "sales_person_id": "SALES_PERSON_ID",
			  "date" : "DATE"
			}' "http://cstore.local/xposrest/route/"

		Example Request

			curl -X POST -H "Content-Type: application/json" -d '{
			  "device_id": "Minh'\''s Ipad",
			  "device_key": "fcea920f7412b5da7be0cf42b8c93759",
			  "sales_person_id": "149",
			  "date" : "27/2/2017"
			}' "http://cstore.local/xposrest/route/"

		Example Response:

			{
			  "status": "success",
			  "result": [
			    {
			      "station_id": "1",
			      "route_id": "1",
			      "routename": "test 1",
			      "day": "Sunday",
			      "salesperson_name": "120",
			      "total_stop": "2",
			      "stops": {
			        "totalRecords": 1,
			        "items": [
			          {
			            "stop_id": "1",
			            "sortorder": "1",
			            "stopname": "RYLAND TEXACO",
			            "route_id": "1",
			            "salesperson_name": "120",
			            "day": "Sunday",
			            "address": null,
			            "amount" : "10.00"
			          },
			          {
			            "stop_id": "2",
			            "sortorder": "2",
			            "stopname": "GREENBRIER FUEL CITY",
			            "route_id": "1",
			            "salesperson_name": "120",
			            "day": "Sunday",
			            "address": null,
			            "amount" : 0
			          }
			        ]
			      },
			      "amount" : "10.00"
			    }
			  ],
			  "message": ""
			}
